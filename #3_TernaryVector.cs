using System;
using System.Collections;
using System.Collections.Generic;


public class TernaryVector : IEnumerable<byte>
{

    private List<byte> _ternaryVector;
    // Review TK: You could just write Count {get {return List.Count;}}
    public int Count {get; private set;}

    public TernaryVector()
    {
        _ternaryVector = new List<byte>();
        Count = 0;
    }

    public void Add(byte item)
    {
        if (item > 2)
        {
            throw new TypeInitializationException("Value must be in {0,1,2}.", null);
        }

        _ternaryVector.Add(item);
        Count++;
    }

    public IEnumerator<byte> GetEnumerator()
    {
        return _ternaryVector.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return this.GetEnumerator();
    }
    // Review TK: Please take into consideration naming for methods.
    // http://se.inf.ethz.ch/old/teaching/ss2007/251-0290-00/project/CSharpCodingStandards.pdf
    // https://msdn.microsoft.com/en-us/library/4df752aw(v=vs.71).aspx
    public int TwosCount()
    {
        int count = 0;
        // Review TK: You could just write return this.Count(item => item == 2);
        foreach (var item in this)
        {
            if (item == 2)
            {
                count++;
            }
        }

        return count;
    }
     // Review TK: Don't forget about naming for methods.
    // Use var if type of variable is clear from the context.
    // This operation is valid for only none orthogonal vectors.
    // You could use MIN operation in order to avoid so a lot of if statements.
    // It is a good practice to validate input parameters.
    public static TernaryVector Cross(TernaryVector v1, TernaryVector v2)
    {
        if (v1.Count != v2.Count)
        {
            throw new ArgumentException("Vectors must be the same length.");
        }

        TernaryVector resultVector = new TernaryVector();

        var item2 = v2.GetEnumerator();
        try
        {
            foreach (var item1 in v1)
            {
                item2.MoveNext();

                if (item1 == 0 || item2.Current == 0)
                {
                    resultVector.Add(0);
                }
                else if (item1 == 1 || item2.Current == 1)
                {
                    resultVector.Add(1);
                }
                else
                {
                    resultVector.Add(2);
                }
            }
        }
        finally
        {
            item2.Dispose();
        }

        return resultVector;
    }


    public TernaryVector Cross(TernaryVector v)
    {
        return TernaryVector.Cross(this, v);
    }

    // Review TK: It is a good practice to validate input parameters.
    public static bool IsOrtogonal(TernaryVector v1, TernaryVector v2)
    {
        if (v1.Count != v2.Count)
        {
            return false;
        }
        else
        {
            int scalar = 0;

            var item2 = v2.GetEnumerator();
            try
            {
                foreach (var item1 in v1)
                {
                    item2.MoveNext();
                    scalar += item1 * item2.Current;
                }
            }
            finally
            {
                item2.Dispose();
            }   
            return scalar == 0;
        }
    }

    public bool IsOrtogonal(TernaryVector v)
    {
        return TernaryVector.IsOrtogonal (this, v);
    }
} 
