using System;

// Review TK: Use appropriate access modifiers before class.
// General objections: Use more meaningful names for methods.
class Matrix
{
    #region Fields 

        private double[,] _element;
        public uint RowLength {get; private set;}
        public uint ColumnLength {get; private set;}

    #endregion 

    public double this[uint i, uint j]
    {
        get { return _element[i, j]; }
        // Review TK: Setting value of elements shouldn't be public.
        set { _element[i, j] = value; }
    }
    
    #region Constructors

        public Matrix (double [,] inputMatrix)
        {
            if (inputMatrix.Rank != 2)
            {
                throw new TypeInitializationException ("Array must be two-dimensional.", null);
            }
            RowLength = (uint)inputMatrix.GetLength(0);
            ColumnLength = (uint)inputMatrix.GetLength(1);

            _element = new double [RowLength,ColumnLength];

            for (int i = 0; i < RowLength; i++)
            {
                for (int j = 0; j < ColumnLength; j++)
                {
                    _element[i,j] = inputMatrix[i,j];
                }
            }
        }

        public Matrix (uint m, uint n)
        {
            RowLength = m;
            ColumnLength = n;
            _element = new double [RowLength, ColumnLength];
        }

        public Matrix (uint m):this(m, m)
        {}

    #endregion 
    
    // Review TK: Give methods names that are verbs or verb phrases.
    #region Methods
        // Review TK: There shouldn't be space between method name and opening parenthesis.
        private double Determinant ()
        {
            double result = 0;
            int sign = 1;
            // Review TK: There are no need in using else statements, as return will break method execution.
            if (this.RowLength == 1)
            {
                return this[0,0];
            }
            else if (this.RowLength == 2)
            {
                double result1 = this [0,0] * this[1,1] - this[0,1] * this[1,0];
                return this [0,0] * this[1,1] - this[0,1] * this[1,0];
            }
            else
            {
                for (uint j = 0; j < ColumnLength; j ++)
                {
                    result += sign * this[0,j] * MinorOfElement(1, j + 1); //Fix
                    sign *= -1;
                }
            }
            // Review TK: I would prefer to add empty space line before return statement.
            return result;
        }
        // Review TK: It should be defined that number of row and column starts with 1.
		// Review TK: You can reuse AdditionalMinor method here.
        // Use more meaningful names for methods.
        // https://msdn.microsoft.com/en-us/library/4df752aw(v=vs.71).aspx
        // http://se.inf.ethz.ch/old/teaching/ss2007/251-0290-00/project/CSharpCodingStandards.pdf
        // Why do you use uint type?
        // Don't use some extra spaces.
        public  double MinorOfElement (uint i, uint j)
        {   
            if (RowLength != ColumnLength)
            {
                throw new InvalidOperationException("The number of rows is not equal the number of columns.");
            }
            Matrix minor = new Matrix(RowLength - 1);
            uint minorRowIndex = 0;

            for (uint row = 0; row < RowLength; row++)
            {
                if (row == i - 1)
                {
                    continue;
                }

                uint minorColumnIndex = 0;

                for (uint col = 0; col < ColumnLength; col++)
                {
                    if (col == j - 1)
                    {
                        continue;
                    }

                    minor [minorRowIndex, minorColumnIndex] = this [row, col];
                    minorColumnIndex++;
                }
                minorRowIndex++;
            }

            return minor.Determinant();
        }

        public double MinorK(uint [] rows, uint [] columns) 
        {
            if (rows.Length != columns.Length)
            {
                throw new ArgumentException("The number of rows is not equal the number of columns.");
            }
            if (rows.Length > RowLength || columns.Length > ColumnLength)
            {
                throw new ArgumentException ("Minor dimension of minor should not be greater than the dimension of the matrix.");
            }

            Matrix minor = new Matrix((uint)rows.Length);
            Array.Sort(rows);
            Array.Sort(columns);

            for (uint i = 0; i < rows.Length; i++)
            {   
                if (i != (rows.Length - 1) && rows [i] == rows [i+1])
                {
                    throw new ArgumentException ("Duplicated minor's row (column)");
                }

                for (uint j = 0; j < rows.Length; j++)
                {
                    if (j != (rows.Length - 1) && columns [j] == columns [j+1])
                    {
                        throw new ArgumentException ("Duplicated minor's row (column)");
                    }

                    minor [i ,j] = this [rows[i] - 1, columns [j] - 1];
                }
            }
            return minor.Determinant();
        }

        public double AdditionalMinor (uint [] rows, uint [] columns)
        {
            if (RowLength != ColumnLength)
            {
                throw new InvalidOperationException("The number of rows is not equal the number of columns.");
            }
            if (rows.Length != columns.Length)
            {
                throw new ArgumentException("The number of rows is not equal the number of columns.");
            }
            if (rows.Length > RowLength || columns.Length > ColumnLength)
            {
                throw new ArgumentException ("Minor dimension of minor should not be greater than the dimension of the matrix.");
            }

            Matrix minor = new Matrix((uint) (this.RowLength - rows.Length));
            Array.Sort(rows);
            Array.Sort(columns);

            uint matrixRowIndex;
            uint matrixColumnIndex;

            uint minorRowIndex = 0;
            uint rowsIndex = 0;

            for (matrixRowIndex = 0; matrixRowIndex < RowLength; matrixRowIndex++)
            {   
                if (rowsIndex != (rows.Length - 1) && rows [rowsIndex] == rows [rowsIndex + 1])
                {
                    throw new ArgumentException ("Duplicated minor's row (column)");
                }
                if (matrixRowIndex == rows [rowsIndex] - 1)
                {
                    if (rowsIndex != rows.Length - 1)
                    {
                        rowsIndex++;
                    }
                    continue;
                }

                uint minorColumnIndex = 0;
                uint columnsIndex = 0;

                for (matrixColumnIndex = 0; matrixColumnIndex < ColumnLength; matrixColumnIndex++)
                {
                    if (columnsIndex != (rows.Length - 1) && columns [columnsIndex] == columns [columnsIndex + 1])
                    {
                        throw new ArgumentException ("Duplicated minor's row (column)");
                    }
                    if (matrixColumnIndex == columns [columnsIndex] - 1)
                    {
                        if (columnsIndex != columns.Length - 1)
                        {
                            columnsIndex++;
                        }
                        continue;
                    }
                    
                    minor [minorRowIndex, minorColumnIndex] = this [matrixRowIndex, matrixColumnIndex];
                
                    minorColumnIndex++;
                }

                minorRowIndex++;
            }
            return minor.Determinant();
        }


    #endregion 
    
}

