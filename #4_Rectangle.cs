using System;

public struct Point
{
    public double X {get;set;}
    public double Y {get;set;}

    public Point (double x, double y)
    {
        // Review TK: This code isn't compilable. 
        // Please use next statement
        /*
        public Point(double x, double y) : this()
        {
            this.X = x;
            this.Y = y;
        }
        */
        this.X = x;
        this.Y = y;
    }

    public Point (double x):this(x,x)
    {}

    public override string ToString()
    {
        // Review TK: I prefer to write string.Format....
        return String.Format("({0};{1})",X,Y);
    }
}

public class Rectangle
{

    #region Fields
        // Review TK: I prefer next order for class's members
        // http://stackoverflow.com/questions/150479/order-of-items-in-classes-fields-properties-constructors-methods
        private Point _bottomLeftCorner;
        private Point _upperRightCorner;
        // Review TK: Please write {} on separated lines.
        public double Width { get { return _upperRightCorner.X - _bottomLeftCorner.X;} }
        public double Heigth { get { return _upperRightCorner.Y - _bottomLeftCorner.Y;} }

    #endregion Fields

    #region Constructors

        public Rectangle (Point p1, Point p2)
        {
            _bottomLeftCorner.X = Math.Min(p1.X, p2.X);
            _bottomLeftCorner.Y = Math.Min(p1.Y, p2.Y);
            _upperRightCorner.X = Math.Max(p1.X, p2.X);
            _upperRightCorner.Y = Math.Max(p1.Y, p2.Y);
        }
               
        public Rectangle (Point p, double width, double heigth)
        {
            _bottomLeftCorner = p;
            _upperRightCorner = new Point (p.X + width, p.Y + heigth);
        }

        public Rectangle (Point p, double width):this(p, width, width)
        {}

        public Rectangle (Point p)
        {
            _bottomLeftCorner = _upperRightCorner = p;
        }

        public Rectangle ()
        {
            _bottomLeftCorner = _upperRightCorner = new Point();
        }
    #endregion Constructors

    #region Methods

        public void Move(double x, double y)
        {
            Point point = new Point(x, y);
            _bottomLeftCorner.X += point.X;
            _bottomLeftCorner.Y += point.Y;
            _upperRightCorner.X += point.X;
            _upperRightCorner.Y += point.Y;
        }
        
        public void Resize(double x, double y)
        {
            _upperRightCorner.X = _bottomLeftCorner.X + x;
            _upperRightCorner.Y = _bottomLeftCorner.Y + y;
        }

        public override string ToString()
        {
            return String.Format ("{0}x{1} from {2}", Width, Heigth, _bottomLeftCorner);
        }
        
        // Review TK: It is a good practice to validate input parameters.
        public static Rectangle Combine(Rectangle r1, Rectangle r2)
        {
            Rectangle resultRectangle = new Rectangle();

            resultRectangle._bottomLeftCorner = new Point(Math.Min(r1._bottomLeftCorner.X, r2._bottomLeftCorner.X), Math.Min(r1._bottomLeftCorner.Y, r2._bottomLeftCorner.Y));
            resultRectangle._upperRightCorner = new Point(Math.Max(r1._upperRightCorner.X, r2._upperRightCorner.X), Math.Max(r1._upperRightCorner.Y, r2._upperRightCorner.Y));
            
            return resultRectangle;
        }

        public Rectangle Combine(Rectangle r)
        {
            return Rectangle.Combine (this, r);
        }
        // Review TK: It is a good practice to validate input parameters.
        // Use var if type of variable is clear from the context.
        public static Rectangle Cross(Rectangle r1, Rectangle r2)
        {
            Rectangle newRectangle = new Rectangle();

            newRectangle._bottomLeftCorner = new Point(Math.Max(r1._bottomLeftCorner.X, r2._bottomLeftCorner.X), Math.Max(r1._bottomLeftCorner.Y, r2._bottomLeftCorner.Y));
            newRectangle._upperRightCorner = new Point(Math.Min(r1._upperRightCorner.X, r2._upperRightCorner.X), Math.Min(r1._upperRightCorner.Y, r2._upperRightCorner.Y));
            
            // Review TK: Ypou could use ternary operator.
            //Check wether rectangles intersect
            if (newRectangle._bottomLeftCorner.X < newRectangle._upperRightCorner.X || newRectangle._bottomLeftCorner.Y < newRectangle._upperRightCorner.Y)
            {
                return newRectangle;
            }
            else
            {
                return null;
            }
        }

        public  Rectangle Cross(Rectangle r)
        {
            return Rectangle.Cross(this, r);
        }
    #endregion Methods

}