using System;
using System.Text;

public class Polynomial
{
    #region Fields
        
        private double[] _coefficient;
        private int _degree;

    #endregion 

    #region Constructors
        // Review TK: What is d? I suppose degree.
        // A lot of work within constructor.  It could be simmple.
        public Polynomial(int d, double[] cof)
        {
        // If the negative power, read coefficients in reverse order;
            if (d > 0)
            {
                _degree = d;

                if ((_degree + 1) != cof.Length)
                {
                    throw new TypeInitializationException("Incorrect degree for a given set of coefficients", null);
                }
                    
                //Ignore zero rates;
                while (cof[_degree] == 0)
                {
                    _degree--;
                }

                _coefficient = new double[_degree + 1];

                for (int i = 0; i <= _degree; i++)
                {
                    _coefficient[i] = cof[i];
                }
            }
            else
            {
                _degree = -d;

                if ((_degree + 1) != cof.Length)
                {
                    throw new TypeInitializationException("Incorrect degree for a given set of coefficients", null);
                }

                //Ignore zero rates;
                while (cof[_degree] == 0)
                {
                    _degree--;
                }

                _coefficient = new double[_degree + 1];

                for(int i = 0, j = _degree; i <= _degree; i++, j--)
                {
                    _coefficient[i] = cof[j];
                }
            }
        }
         // Review TK: {} should be on separated lines.
        public Polynomial(double[] cof):this(cof.Length - 1, cof)
        {}
    
    #endregion 

    #region Methods
        // Review TK: It is a good practice to validate input parameters.
        // Use var if type of variable is clear from the context.
        public static Polynomial operator+ (Polynomial p1, Polynomial p2)
        {
            int newDegree = Math.Max(p1._degree, p2._degree);
            double[] newCoefficients = new double[newDegree + 1];
            
            for (int i = 0; i <= p1._degree; i++)
            {
                newCoefficients[i] += p1._coefficient[i];
            }

            for (int i = 0; i <= p2._degree; i++)
            {
                newCoefficients[i] += p2._coefficient[i];
            }

            return new Polynomial (newDegree, newCoefficients);
        }
         // Review TK: The same comments as for previous method.
        public static Polynomial operator- (Polynomial p1, Polynomial p2)
        {
            int newDegree = Math.Max(p1._degree, p2._degree);
            double[] newCoefficients = new double[newDegree + 1];
            
            for (int i = 0; i <= p1._degree; i++)
            {
                newCoefficients[i] += p1._coefficient[i];
            }

            for (int i = 0; i <= p2._degree; i++)
            {
                newCoefficients[i] -= p2._coefficient[i];
            }

            return new Polynomial (newDegree, newCoefficients);
        }

        public static Polynomial operator* (Polynomial p1, Polynomial p2)
        {
            int newDegree = p1._degree + p2._degree;
            double[] newCoefficients = new double[newDegree + 1];
            
            for(int i = 0; i <= p1._degree; i++)
            {
                for (int j = 0; j <= p2._degree; j++)
                {
                    newCoefficients [i+j] += p1._coefficient[i] * p2._coefficient[j];
                }
            }
            // Review TK: I prefer to use empty line space before return statement.
            return new Polynomial (newDegree, newCoefficients);
        }

        // Review TK: A lot of if statements. Perhaps ternary operator could help or even switch statement.
        public override string ToString()
        {
            StringBuilder result = new StringBuilder();
            
            for (int i = _degree; i >= 0; i--)
            {
                //Ignore a sign of the highest degree;
                if (i == _degree)
                {
                    result.Append(String.Format("{0}X^{1}", _coefficient[i], i));
                }
                //Ignore zero values;
                else if (_coefficient[i] == 0 )
                {
                     continue;
                }
                else if (i == 1)  
                {
                    result.Append(String.Format("{0}X", _coefficient[i].ToString("+#;-#;0")));
                }
                else if (i == 0)
                {
                    result.Append(String.Format("{0}", _coefficient[i].ToString("+#;-#;0")));
                }
                else
                {
                    result.Append(String.Format("{0}X^{1}", _coefficient[i].ToString("+#;-#;0"), i));
                }
            }

            return result.ToString();
        }

                
        public double Calculate(double x)
        {
            return Polynomial.Calculate(this, x);
        }

        public static double Calculate(Polynomial p, double x)
        {
                double result = 0;
                
                for (int i = 0; i < p._degree; i++)
                {
                    result += p._coefficient[i] * Math.Pow(x, i);
                }

                return result;
        }

    #endregion 

}




