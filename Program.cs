using System;

class Program
{
    static void Main(string[] args)
    {
        Console.WriteLine("---------------------\nTask#1 demonstration:\n");
        Matrix A = new Matrix (new double[,]{{1,1,2,4},{1,1,3,5},{1,2,1,6},{6,1,2,5}});

        Console.WriteLine("Mtrix A:" );

        for (uint i = 0; i < A.RowLength; i++)
        {
            for (uint j = 0; j < A.ColumnLength; j++)
            {
                Console.Write("{0}, ", A[i,j] );
            }
            Console.WriteLine();
        }

        Console.Write("Minor of element A[1,1]:" );
        Console.WriteLine(A.MinorOfElement(1,1));

        Console.Write("Minor K of matrix A:" );
        Console.WriteLine(A.MinorK(new uint[]{3,2,1}, new uint[]{1,2,3}));
        
        Console.Write("Aadditional minor K of matrix A:" );
        Console.WriteLine(A.AdditionalMinor(new uint[]{3,2,1}, new uint[]{1,2,3}));
       

        Console.WriteLine("\n---------------------\nTask#2 demonstration:\n");
        Polynomial p1 = new Polynomial (-3, new double[]{1,2,1,1});
        Polynomial p2 = new Polynomial (new double[]{0,1,1});
        
        Console.WriteLine("p1 = {0}", p1);
        Console.WriteLine("p2 = {0}",p2);
        Console.WriteLine("p1 + p2 = {0}", p1+p2);
        Console.WriteLine("p1 - p2 = {0}", p1-p2);
        Console.WriteLine("p1 * p2 = {0}", p1*p2);
         // Review TK: It seems this method doesn't work correctly.
         // You use hardcode value 0.5, you write p2 however calculations are for p1.
        Console.WriteLine("p2(0.5) = {0}", p1.Calculate(0.5));


        Console.WriteLine("\n---------------------\nTask#3 demonstration:\n");
        TernaryVector v1 = new TernaryVector(){1,2,0,2};
        TernaryVector v2 = new TernaryVector(){0,0,1,0};
        TernaryVector v3 = new TernaryVector(){1,0,1,2};

        Console.Write ("v1 = ");
        foreach (var item in v1)
        {
            Console.Write("{0}, ", item);
        }
        Console.WriteLine();

        Console.Write ("v2 = ");
        foreach (var item in v2)
        {
            Console.Write("{0}, ", item);
        }
        Console.WriteLine();

        Console.Write ("v3 = ");
        foreach (var item in v3)
        {
            Console.Write("{0}, ", item);
        }
        Console.WriteLine();

        Console.WriteLine("v1 ortogonal v2: {0}", v1.IsOrtogonal(v2));

        Console.Write("v1 cross v3: ");
        foreach (var item in v1.Cross(v3))
        {
            Console.Write("{0}, ", item);
        }
        Console.WriteLine();
        
        Console.WriteLine("Number of v1 elements equal \"2\": {0}", v1.TwosCount());

        // Review TK: Your demo isn't full.
        Console.WriteLine("\n---------------------\nTask#4 demonstration:\n");
        Rectangle r1 = new Rectangle(new Point (1), 2);
        Rectangle r2 = new Rectangle(new Point (0), 1.5);
        
        Console.WriteLine("r1: {0}", r1);
        Console.WriteLine("r2: {0}", r2);
        Console.WriteLine("r1 cross r2: {0}", r1.Cross(r2));
        Console.WriteLine("r1 union r2: {0}", r1.Combine(r2));
    }

}